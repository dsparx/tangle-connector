# Switch us to current script path and move two folders up
$script_path = $MyInvocation.MyCommand.Path
$windows_dir = Split-Path $script_path
$project_root_dir = "$windows_dir\..\.."
cd $project_root_dir

Start-Process powershell -verb runas -ArgumentList "-file $windows_dir\lcov_dependencies_install.ps1" -Wait
refreshenv # We need to run this, because if the perl/choco is installed in previous step, the environmental variables change

Write-Output "Be patient please, this may take couple seconds..."

$flutter_test_command = "flutter test --coverage"
Invoke-Expression $flutter_test_command -OutVariable result # Done this way because we want to see the output in terminal

$path_to_genhtml = "C:\ProgramData\chocolatey\lib\lcov\tools\bin\genhtml"
# Calling the GENHTML script
perl $path_to_genhtml -o coverage\html coverage\lcov.info

# Add a README.md to the coverage directory
$open_index_html = "Open index.html in any browser to display the lcov (test coverage) report."
Write-Output $open_index_html | Out-File -FilePath "coverage/README.md"

# Print to the console in the end
Write-Output "`n$open_index_html"

Exit $result
