import 'package:flutter/foundation.dart';
import 'package:location/location.dart';

class GpsService {
  //final _log = getLogger('GpsServiceImpl');
  final Location locationService;

  GpsService({required this.locationService});

  factory GpsService.instance() => GpsService(locationService: Location());

  Future<void> ensurePermissionGrantedAndServiceEnabled() async {
    if (defaultTargetPlatform == TargetPlatform.android) {
      await ensurePermissionGranted();
      await ensureServiceEnabled();
    }
  }

  Future<void> ensurePermissionGranted() async {
    //_log.i('Ensuring permission granted');
    final bool permissionStatus = await _isPermissionGranted();
    if (!permissionStatus) {
      //_log.d('Permission NOT granted -> Requesting permission..');
      final bool permissionGranted = await _requestPermission();
      if (!permissionGranted) throw StateError('GPS Permission not granted');
    }
    //_log.d('Permission granted');
  }

  Future<void> ensureServiceEnabled() async {
    //_log.i('Ensuring service enabled on device');
    final bool serviceStatus = await locationService.serviceEnabled();
    if (!serviceStatus) {
      //_log.d('Service NOT enabled -> Requesting service..');
      final bool serviceEnabled = await locationService.requestService();
      if (!serviceEnabled) throw StateError('GPS not enabled');
    }
    //_log.d('Service enabled');
  }

  Future<bool> _isPermissionGranted() async {
    final PermissionStatus permissionStatus = await locationService.hasPermission();
    return permissionStatus == PermissionStatus.granted || permissionStatus == PermissionStatus.grantedLimited;
  }

  Future<bool> _requestPermission() async {
    final PermissionStatus permissionStatus = await locationService.requestPermission();
    return permissionStatus == PermissionStatus.granted || permissionStatus == PermissionStatus.grantedLimited;
  }
}
