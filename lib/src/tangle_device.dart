import 'package:convert/convert.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/manufacturer_data.dart';
import 'package:flutter_connector/src/utils/mtu_analyzer.dart';

class TangleDevice {
  //final _log = getLogger('TangleDevice');
  final int discoverServicesTimeout;
  final int writeTimeout;

  final Guid serviceUUID;
  final Guid networkCharacteristicUUID;
  final Guid clockCharacteristicUUID;
  final Guid deviceCharacteristicUUID;

  final BluetoothDevice device;
  final String name;
  final ManufacturerData manufacturerData;
  final int rssi;
  int _mtu = 0;
  BluetoothService? _service;
  BluetoothCharacteristic? _networkCharacteristic;
  BluetoothCharacteristic? _clockCharacteristic;
  BluetoothCharacteristic? _deviceCharacteristic;

  TangleDevice({
    required ScanResult scanResult,
    required this.serviceUUID,
    required this.networkCharacteristicUUID,
    required this.clockCharacteristicUUID,
    required this.deviceCharacteristicUUID,
    required this.discoverServicesTimeout,
    required this.writeTimeout,
  })  : device = scanResult.device,
        // result.device.name appears to be cached by iOS, causing issues after renaming during adoption
        // result.advertisementData.localName should not be cached and therefore always matching the reality
        name = scanResult.advertisementData.localName,
        manufacturerData = ManufacturerData.fromData(scanResult.advertisementData.manufacturerData),
        rssi = scanResult.rssi;

  Future<void> connect() async {
    await disconnect();
    //_log.d('Connecting');
    await device.connect();
    await _findAndSetService(device);
    await setupMtu();
    _setCharacteristics();
  }

  Future<void> disconnect() async {
    //_log.d('Disconnecting');
    await device.disconnect();
    _service = null;
    _networkCharacteristic = null;
    _clockCharacteristic = null;
    _deviceCharacteristic = null;
  }

  Future<void> writeNetwork(Uint8List bytes, {bool withoutResponse = false, int? timeout}) {
    return _write(
      _networkCharacteristic!,
      'network',
      bytes,
      withoutResponse: withoutResponse,
      timeout: timeout,
    );
  }

  Future<void> writeClock(Uint8List bytes, {bool withoutResponse = false, int? timeout}) {
    return _write(
      _clockCharacteristic!,
      'clock',
      bytes,
      withoutResponse: withoutResponse,
      timeout: timeout,
    );
  }

  Future<void> writeDevice(Uint8List bytes, {bool withoutResponse = false, int? timeout}) {
    return _write(
      _deviceCharacteristic!,
      'device',
      bytes,
      withoutResponse: withoutResponse,
      timeout: timeout,
    );
  }

  Future<List<int>> readNetwork() {
    return _read(_networkCharacteristic!, 'network');
  }

  Future<List<int>> readClock() {
    return _read(_clockCharacteristic!, 'clock');
  }

  Future<List<int>> readDevice() {
    return _read(_deviceCharacteristic!, 'device');
  }

  Future<Stream<List<int>>> streamNetwork() async {
    return await _listen(_networkCharacteristic!, 'network');
  }

  Future<Stream<List<int>>> streamClock() async {
    // Surround with try-catch because clock notifications are supported from FW 0.9.0
    try {
      return await _listen(_clockCharacteristic!, 'clock');
    } catch (e) {
      return const Stream.empty();
    }
  }

  Future<Stream<List<int>>> streamDevice() async {
    return await _listen(_deviceCharacteristic!, 'device');
  }

  Future<void> setupMtu() async {
    device.mtu.listen((newMtu) async {
      final upperBoundMtu = await MtuAnalyzer().mtuWithPlatformUpperBound(newMtu);
      //_log.d('Setting MTU = $upperBoundMtu');
      _mtu = upperBoundMtu;
    });

    // MTU request is made automatically by iOS
    if (defaultTargetPlatform == TargetPlatform.android) {
      const desiredMtu = 512; // Solve the MTU value inside listener
      //_log.d('Requesting new MTU for Android: $desiredMtu bytes');
      await device.requestMtu(desiredMtu);
      //_log.d('Rediscovering services after MTU request');
      await _findAndSetService(device);
    }
  }

  Future<void> _write(BluetoothCharacteristic characteristic, String charName, Uint8List bytes,
      {required bool withoutResponse, required int? timeout}) async {
    //_log.d('Writing bytes to $charName characteristic:');
    //_log.d(bytes);
    if (kDebugMode) {
      print('Writing ${bytes.length} bytes to $charName characteristic');
    }
    try {
      if (defaultTargetPlatform == TargetPlatform.iOS && withoutResponse == true) {
        const msg = 'iOS does not support write WITHOUT response! Write WITH response instead';
        //_log.e(msg);
        throw msg;
      }
      return characteristic
          .write(bytes, withoutResponse: withoutResponse)
          .timeout(Duration(milliseconds: timeout ?? writeTimeout));
    } catch (e) {
      final msg = 'Write to $charName characteristic failed!';
      //_log.e(msg);
      throw Exception(msg);
    }
  }

  Future<List<int>> _read(BluetoothCharacteristic characteristic, String charName) async {
    //_log.d('Reading bytes from $charName characteristic:');
    try {
      final List<int> buffer = [];
      while (true) {
        final res = await characteristic.read();
        buffer.addAll(res);
        if (res.length != mtu) break;
      }
      return buffer;
    } catch (e) {
      final msg = 'Reading from $charName characteristic failed!';
      //_log.e(msg);
      throw Exception(msg);
    }
  }

  Future<Stream<List<int>>> _listen(BluetoothCharacteristic characteristic, String charName) async {
    //_log.d('Listening to $charName characteristic:');
    await characteristic.setNotifyValue(true);
    return characteristic.value;
  }

  Future<void> _findAndSetService(BluetoothDevice device) async {
    //_log.d('Discovering services');
    final List<BluetoothService> services;
    try {
      // Needs around 5s sometimes to complete
      services = await device.discoverServices().timeout(Duration(milliseconds: discoverServicesTimeout));
    } catch (e) {
      const msg = 'Discover services timeout!';
      //_log.e(msg);
      throw Exception(msg);
    }
    _setService(services);
  }

  void _setService(List<BluetoothService> services) {
    if (services.isEmpty) {
      throw Exception('Device services are empty');
    }
    for (final BluetoothService service in services) {
      if (service.uuid == serviceUUID) {
        _service = service;
        return;
      }
    }
    throw Exception('Service not found in device');
  }

  void _setCharacteristics() {
    for (final BluetoothCharacteristic char in _service!.characteristics) {
      if (char.uuid == networkCharacteristicUUID) {
        _networkCharacteristic = char;
      }
      if (char.uuid == clockCharacteristicUUID) {
        _clockCharacteristic = char;
      }
      if (char.uuid == deviceCharacteristicUUID) {
        _deviceCharacteristic = char;
      }
    }
    if (_networkCharacteristic == null || _clockCharacteristic == null || _deviceCharacteristic == null) {
      throw Exception('Characteristics not found in device');
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "namePrefix": "",
      "fwVersion": manufacturerData.fwVersion,
      "macAddress": id,
      "ownerSignature": hex.encode(manufacturerData.ownerSignatureHash!),
      "productCode": manufacturerData.productCode,
      "adoptionFlag": manufacturerData.adoption,
      "legacy": false
    };
  }

  String get id => device.id.id;

  int get mtu => _mtu;

  Stream<BluetoothDeviceState> get state => device.state;
}
