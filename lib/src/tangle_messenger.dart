import 'package:flutter_connector/flutter_connector.dart';

class TangleMessenger {
  Future<TangleDevice?> userSelect(FilterData criteria, int timeout) async => null;

  Future<TangleDevice?> autoSelect(FilterData criteria, int scanPeriod, int timeout) async => null;

  void resolve() {}

  void resolveDeviceString(TangleDevice device) {}

  void resolveString(String value) {}

  void resolveBytes(List<int> bytes) {}

  void reject() {}

  void rejectAs(String errorMessage) {}

  void emitConnected() {}

  void emitDisconnected() {}

  void emitOtaStatus(String value) {}

  void emitOtaProgress(int percentage) {}

  void notifyNetwork(List<int> bytes) {}

  void notifyClock(List<int> bytes) {}

  void notifyDevice(List<int> bytes) {}
}
