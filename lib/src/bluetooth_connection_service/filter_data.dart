import 'dart:typed_data';

import 'package:convert/convert.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/manufacturer_data.dart';

class FilterData {
  final Iterable<FilterItem> filters;

  FilterData({
    required this.filters,
  });

  factory FilterData.empty() => FilterData(filters: []);

  factory FilterData.fromJson(List<dynamic> jsonArray) {
    final filtersJson = jsonArray.map((e) => e as Map<String, dynamic>).toList();
    final withoutLegacy = filtersJson.where((json) => !json.containsKey('legacy'));
    return FilterData(
      filters: withoutLegacy.map(FilterItem.fromJson),
    );
  }
}

class FilterItem {
  final String? name;
  final String? namePrefix;
  final Set<DeviceIdentifier>? restrictedIdentifiers;
  final ManufacturerData? manufacturerData;

  FilterItem({
    this.name,
    this.namePrefix,
    this.restrictedIdentifiers,
    this.manufacturerData,
  });

  factory FilterItem.fromJson(Map<String, dynamic> json) {
    final ownerSignature = json['ownerSignature'] as String?;
    final bytes = ownerSignature == null ? null : Uint8List.fromList(hex.decode(ownerSignature));
    return FilterItem(
      name: json['name'] as String?,
      namePrefix: json['namePrefix'] as String?,
      manufacturerData: ManufacturerData(
        fwVersion: json['fwVersion'] as String?,
        productCode: json['productCode'] as int?,
        ownerSignatureHash: bytes,
        adoption: json['adoptionFlag'] as bool?,
      ),
    );
  }
}
