import 'package:flutter/foundation.dart';

class ManufacturerData {
  static const fwVersionOffset = 0;
  static const productCodeOffset = 2;
  static const ownerSignatureOffset = 4;
  static const otherFlagsOffset = 20;

  final String? fwVersion;
  final int? productCode;
  final Uint8List? ownerSignatureHash;
  final bool? adoption;

  ManufacturerData({
    this.fwVersion,
    this.productCode,
    this.ownerSignatureHash,
    this.adoption,
  });

  static int bytes16ToCode(List<int> bytes) => bytes[0] + (bytes[1] << 8);

  static String bytesToFwVersion(List<int> bytes) {
    final versionCode = bytes16ToCode(bytes);
    final thousands = (versionCode / 1000).floor();
    final hundreds = ((versionCode - 1000 * thousands) / 100).floor();
    final decimals = versionCode - 1000 * thousands - 100 * hundreds;
    return '$thousands.$hundreds.$decimals';
  }

  factory ManufacturerData.fromData(Map<int, List<int>> manufacturerData) {
    final data = manufacturerData.values.first;
    assert(data.length == 21);

    final fwBytes = data.sublist(fwVersionOffset, productCodeOffset);
    final productCodeBytes = data.sublist(productCodeOffset, ownerSignatureOffset);
    final ownerSignatureBytes = data.sublist(ownerSignatureOffset, otherFlagsOffset);
    final adoptionFlag = data[otherFlagsOffset];

    return ManufacturerData(
      fwVersion: bytesToFwVersion(fwBytes),
      productCode: bytes16ToCode(productCodeBytes),
      ownerSignatureHash: Uint8List.fromList(ownerSignatureBytes),
      adoption: adoptionFlag == 0 ? false : true,
    );
  }

  bool unmatchesFwVersion(String? fwVersion) {
    if (fwVersion == null) return true;
    return matchesFwVersion('!' + fwVersion);
  }

  bool matchesFwVersion(String? fwVersion) {
    if (fwVersion == null) return true;

    final fwVersionRegexp = RegExp(r'(!?)([\d]+.[\d]+.[\d]+)');
    final firstMatch = fwVersionRegexp.allMatches(fwVersion).first;

    final bool negation = firstMatch.group(1) == '!';
    final String fwCode = firstMatch.group(2)!;

    final equal = this.fwVersion == fwCode;
    return negation ? !equal : equal;
  }

  bool matchesProductCode(int? productCode) {
    if (productCode == null) return true;
    return this.productCode == productCode;
  }

  bool matchesOwnerSignature(Uint8List? ownerSignatureHash) {
    if (ownerSignatureHash == null) return true;

    final ownerSignatureFootprint = ownerSignatureHash.sublist(0, 16);
    return listEquals(this.ownerSignatureHash, ownerSignatureFootprint);
  }

  bool matchesAdoptionFlag(bool? adoption) {
    if (adoption == null) return true;
    return this.adoption == adoption;
  }
}
