import 'dart:math';

import 'package:flutter/foundation.dart';

class ScanTimer {
  // [>= 100] Set on iOS to avoid PlatformException(connect, Peripheral not found, null, null)
  // when scanning after device is turned off manually
  //
  // [>= 500] Set on iOS to avoid PlatformException(connect, Peripheral not found, null, null)
  // on older phones
  // BUG: https://github.com/pauldemarco/flutter_blue/issues/497
  // FIX: https://github.com/pauldemarco/flutter_blue/issues/497#issuecomment-871832581
  // REASON: flutter_blue plugins need some time to add the scanned device to _scannedPeripherals
  static const iosMinScan = 500;

  // [>= 100] Set on Android to avoid Device Services Empty. when calling discoverServices()
  static const androidMinScan = 100;

  final int scanDurationLowerBound = defaultTargetPlatform == TargetPlatform.iOS ? iosMinScan : androidMinScan;
  DateTime scanStart = DateTime.now();
  late int minScanDuration = scanDurationLowerBound;

  void restart({int? minimalScanDuration}) {
    scanStart = DateTime.now();
    minScanDuration = max(scanDurationLowerBound, minimalScanDuration ?? 0);
  }

  bool pastMinimalScanDuration() {
    return _sinceStart() > minScanDuration;
  }

  int _sinceStart() {
    return DateTime.now().millisecondsSinceEpoch - scanStart.millisecondsSinceEpoch;
  }
}
