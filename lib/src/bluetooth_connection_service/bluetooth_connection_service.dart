import 'dart:async';

import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/constants.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/scan_timer.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/tangle_advertisement_filter.dart';
import 'package:flutter_connector/flutter_connector.dart';

abstract class BluetoothConnectionService {
  Stream<Iterable<TangleDevice>> scanResults({required FilterData filterData});

  Future<void> startScan({required int timeout, required bool disconnectConnectedDevices, int? minimalScanDuration});

  Future<void> ensureScanStopped();

  Stream<BluetoothState> get state;

  Stream<bool> get isScanning;
}

class BluetoothConnectionServiceImpl implements BluetoothConnectionService {
  //final _log = getLogger('BluetoothConnectionService');
  final TangleAdvertisementFilter filter = TangleAdvertisementFilterImpl();
  final ScanTimer scanTimer = ScanTimer();

  final int discoverServicesTimeout;
  final Guid serviceGUID;
  final Guid serviceAdoptGUID;
  final Guid networkCharacteristicGUID;
  final Guid clockCharacteristicGUID;
  final Guid deviceCharacteristicGUID;

  final FlutterBluePlus flutterBlue;
  final GpsService gpsService;
  //StreamSubscription? _scanSubscription;

  @override
  late Stream<BluetoothState> state;

  factory BluetoothConnectionServiceImpl.instance({
    required int discoverServicesTimeout,
  }) =>
      BluetoothConnectionServiceImpl(
          flutterBlue: FlutterBluePlus.instance,
          gpsService: GpsService.instance(),
          discoverServicesTimeout: discoverServicesTimeout,
          serviceGUID: Guid(serviceUUID),
          serviceAdoptGUID: Guid(serviceAdoptUUID),
          networkCharacteristicGUID: Guid(networkCharacteristicUUID),
          clockCharacteristicGUID: Guid(clockCharacteristicUUID),
          deviceCharacteristicGUID: Guid(deviceCharacteristicUUID));

  BluetoothConnectionServiceImpl({
    required this.flutterBlue,
    required this.gpsService,
    required this.discoverServicesTimeout,
    required this.serviceGUID,
    required this.serviceAdoptGUID,
    required this.networkCharacteristicGUID,
    required this.clockCharacteristicGUID,
    required this.deviceCharacteristicGUID,
  }) {
    _initMyBluetoothStateStream();
  }

  @override
  Future<void> startScan(
      {required int timeout, required bool disconnectConnectedDevices, int? minimalScanDuration}) async {
    await ensureBluetoothOn();
    await gpsService.ensurePermissionGrantedAndServiceEnabled();
    await ensureScanStopped();

    if (disconnectConnectedDevices) {
      await _disconnectConnectedDevices();
    }
    scanTimer.restart(minimalScanDuration: minimalScanDuration);
    flutterBlue.startScan(
        withServices: [serviceGUID, serviceAdoptGUID /*UUID during adopt state changes*/],
        allowDuplicates: true,
        timeout: Duration(milliseconds: timeout));
  }

  @override
  Future<void> ensureScanStopped() async {
    //_log.i('Stop scanning..');
    final _isScanning = await isScanning.first;
    if (_isScanning) {
      await flutterBlue.stopScan();
      // Add delay to fix bug of calling stopScan(), startScan() fast after another. Sometimes the scan doesn't start
      // 30ms and above helped to fix the bug
      // 20ms and below doesn't fix the bug
      await Future.delayed(const Duration(milliseconds: 50)); // Use 50ms for some margin of safety
    }
  }

  @override
  Stream<Iterable<TangleDevice>> scanResults({required FilterData filterData}) {
    return flutterBlue.scanResults.map((results) {
      final filtered = results.where((result) => filter.verifyScanResult(
            result: result,
            filter: filterData,
          ));
      final devices = filtered.map(_initTangleDevice);
      if (scanTimer.pastMinimalScanDuration()) {
        return devices;
      } else {
        return [];
      }
    });
  }

  void _initMyBluetoothStateStream() {
    state = flutterBlue.state;
  }

  TangleDevice _initTangleDevice(ScanResult scanResult) => TangleDevice(
        scanResult: scanResult,
        serviceUUID: serviceGUID,
        networkCharacteristicUUID: networkCharacteristicGUID,
        clockCharacteristicUUID: clockCharacteristicGUID,
        deviceCharacteristicUUID: deviceCharacteristicGUID,
        discoverServicesTimeout: discoverServicesTimeout,
        writeTimeout: writeTimeout,
      );

  Future<void> ensureBluetoothOn() async {
    final blOn = await flutterBlue.isOn;
    if (!blOn) {
      throw Exception('BluetoothOff');
    }
  }

  Future<void> openPermissionsSettings() async {
    //_log.i('Opening app settings (permissions)');
    await openAppSettings();
  }

  Future<void> _disconnectConnectedDevices() async {
    final connected = await flutterBlue.connectedDevices;
    for (final device in connected) {
      await device.disconnect();
    }
  }

  @override
  Stream<bool> get isScanning => flutterBlue.isScanning;
}
