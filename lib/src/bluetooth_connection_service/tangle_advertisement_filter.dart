import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/filter_data.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/manufacturer_data.dart';

abstract class TangleAdvertisementFilter {
  bool verifyScanResult({required ScanResult result, required FilterData filter});
}

class TangleAdvertisementFilterImpl implements TangleAdvertisementFilter {
  //final _log = getLogger('TangleAdvertisementFilterImpl');

  @override
  bool verifyScanResult({required ScanResult result, required FilterData filter}) {
    if (filter.filters.isEmpty) {
      return verifyManufacturerDataFormat(result.advertisementData.manufacturerData);
    }
    for (final FilterItem filter in filter.filters) {
      if (verifyFilterItem(result: result, filter: filter)) {
        return true;
      }
    }
    return false;
  }

  bool verifyFilterItem({required ScanResult result, required FilterItem filter}) {
    // result.device.name appears to be cached by iOS, causing issues in autoSelect after adoption
    // result.advertisementData.localName should not be cached and therefore always matching the reality
    final deviceName = result.advertisementData.localName;
    return verifyName(name: filter.name, nameToVerify: deviceName) &&
        verifyNamePrefix(prefix: filter.namePrefix, nameToVerify: deviceName) &&
        verifyRestrictedIdentifiers(filter.restrictedIdentifiers, result.device.id) &&
        verifyManufacturerData(
          filter.manufacturerData,
          result.advertisementData.manufacturerData,
        );
  }

  bool verifyName({String? name, required String nameToVerify}) {
    if (name == null) return true;
    return nameToVerify == name;
  }

  bool verifyNamePrefix({String? prefix, required String nameToVerify}) {
    if (prefix == null) return true;
    return nameToVerify.startsWith(prefix);
  }

  bool verifyRestrictedIdentifiers(Set<DeviceIdentifier>? restrictedIdentifiers, DeviceIdentifier idToVerify) {
    if (restrictedIdentifiers == null) return true;
    return restrictedIdentifiers.contains(idToVerify) == false;
  }

  bool verifyManufacturerData(ManufacturerData? manufacturerData, Map<int, List<int>> dataToVerify) {
    //_log.v('ManufacturerData = $dataToVerify');
    if (manufacturerData == null) return true;

    if (verifyManufacturerDataFormat(dataToVerify) == false) {
      return false;
    }

    final manufacturerToVerify = ManufacturerData.fromData(dataToVerify);
    return manufacturerToVerify.matchesFwVersion(manufacturerData.fwVersion) &&
        manufacturerToVerify.matchesProductCode(manufacturerData.productCode) &&
        manufacturerToVerify.matchesOwnerSignature(manufacturerData.ownerSignatureHash) &&
        manufacturerToVerify.matchesAdoptionFlag(manufacturerData.adoption);
  }

  bool verifyManufacturerDataFormat(Map<int, List<int>> data) {
    final values = data.values;
    if (values.isEmpty) return false;
    if (values.first.length != 21) return false;
    return true;
  }
}
