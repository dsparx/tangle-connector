import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_connector/flutter_connector.dart';
import 'package:spectoda_utils/spectoda_utils.dart';

class TangleConnector {
  static const int maxUUID = 4294967294;
  // Use random offset to prevent packet with same UUID if app is restarted
  late int _uuid = random.nextInt(maxUUID);

  final metadataParser = TangleMetadataParser();
  final random = Random();

  late TangleMessenger messenger;
  TangleDevice? device;

  bool _selected = false;
  bool isConnected = false;

  StreamSubscription? networkCharSubscription;
  StreamSubscription? clockCharSubscription;
  StreamSubscription? deviceCharSubscription;
  void Function(List<int> bytes)? handleNetworkNotification;
  void Function(List<int> bytes)? handleClockNotification;
  void Function(List<int> bytes)? handleDeviceNotification;

  StreamSubscription<BluetoothDeviceState>? deviceSubscription;

  Future<void> userSelect(String criteria, int timeout) async {
    _select(() => messenger.userSelect(FilterData.fromJson(jsonDecode(criteria)), timeout), timeout);
  }

  Future<void> autoSelect(String criteria, int scanPeriod, int timeout) async {
    _select(() => messenger.autoSelect(FilterData.fromJson(jsonDecode(criteria)), scanPeriod, timeout), timeout);
  }

  Future<void> _select(Future<TangleDevice?> Function() selectFunction, int timeout) async {
    if (isConnected) {
      //await window.flutter_inappwebview.callHandler("disconnect"); // handle disconnection inside the flutter app
      await ensureDisconnectedWithoutResolve();
    }
    try {
      device = await selectFunction().timeout(Duration(milliseconds: timeout));
      if (device == null) {
        messenger.rejectAs("UserCanceledSelection");
        return;
      }
      _selected = true;
      messenger.resolveDeviceString(device!);
    } catch (e) {
      messenger.rejectAs("SelectionFailed: $e"); // if the selection fails, return "SelectionFailed"
    }
  }

  Future<void> selected() async {
    if (_selected) {
      messenger.resolveDeviceString(device!);
    } else {
      messenger.resolve(); // if no device is selected messenger.resolve nothing
    }
  }

  Future<void> unselect() async {
    if (isConnected) {
      //await window.flutterConnection.disconnect();
      await disconnect();
    }
    device = null;
    _selected = false;
    messenger.resolve();
  }

  Future<void> connect(int timeout) async {
    if (!_selected) {
      messenger.rejectAs("DeviceNotSelected");
      return;
    }
    try {
      await _listenDeviceState();
      await device?.connect().timeout(Duration(milliseconds: timeout));
      await _listenNotifications();
      isConnected = true;
      messenger.emitConnected();
      messenger.resolveDeviceString(device!);
    } catch (e) {
      messenger.rejectAs("ConnectionFailed: $e");
      return;
    }
  }

  Future<void> disconnect() async {
    await ensureDisconnectedWithoutResolve();
    messenger.resolve(); // always messenger.resolves even if there are internal errors
  }

  Future<void> ensureDisconnectedWithoutResolve() async {
    if (isConnected) {
      await device?.disconnect(); // disconnecting logic
      await _cancelCharSubscriptions();
      isConnected = false;
      messenger.emitDisconnected();
    }
  }

  Future<void> connected() async {
    if (isConnected) {
      messenger.resolveDeviceString(device!);
    } else {
      messenger.resolve();
    }
  }

  Future<void> deliver(Uint8List commandPayload, {int? timeout}) async {
    if (!isConnected) {
      messenger.rejectAs("DeviceDisconnected");
      return;
    }
    try {
      await _writeBytes(commandPayload, device!.writeNetwork, timeout: timeout);
      messenger.resolve();
    } catch (e) {
      messenger.rejectAs("DeliverFailed: $e");
    }
  }

  Future<void> transmit(Uint8List commandPayload, {int? timeout}) async {
    if (!isConnected) {
      messenger.rejectAs("DeviceDisconnected");
      return;
    }
    try {
      await _writeBytes(commandPayload, device!.writeNetwork, withoutResponse: true, timeout: timeout);
      messenger.resolve();
    } catch (e) {
      messenger.rejectAs("TransmitFailed: $e");
    }
    messenger.resolve();
  }

  Future<List<int>?> request(Uint8List commandPayload, {required bool readResponse, int? timeout}) async {
    if (!isConnected) {
      messenger.rejectAs("DeviceDisconnected");
      return null;
    }
    try {
      await _writeBytes(commandPayload, device!.writeDevice, timeout: timeout);
      if (readResponse) {
        final response = await device?.readDevice();
        if (response == null) {
          messenger.rejectAs("ResponseIsNull");
        } else {
          messenger.resolveBytes(response);
          return response;
        }
      } else {
        messenger.resolve();
      }
    } catch (e) {
      messenger.rejectAs("RequestFailed: $e");
    }
    return null;
  }

  Future<void> writeClock(Uint8List timestamp) async {
    if (!isConnected) {
      messenger.rejectAs("DeviceDisconnected");
      return;
    }
    try {
      await device!.writeClock(timestamp);
      await _cleanSyncCommunication();
      messenger.resolve();
    } catch (e) {
      messenger.rejectAs("ClockWriteFailed: $e");
    }
  }

  Future<void> readClock() async {
    if (!isConnected) {
      messenger.rejectAs("DeviceDisconnected");
      return;
    }
    try {
      final response = await device?.readClock();
      if (response == null) {
        messenger.rejectAs("ClockResponseIsNull");
      } else {
        messenger.resolveBytes(response); // returns timestamp as an 32-bit signed number
      }
    } catch (e) {
      messenger.rejectAs("ClockReadFailed: $e");
    }
  }

  Stream<int> updateFW(Uint8List bytes) async* {
    if (!isConnected) {
      messenger.rejectAs("DeviceDisconnected");
      return;
    }

    //_log.i('Updating NETWORK firmware');
    final int chunkSize = Platform.isAndroid ? 480 : 4976; // must be modulo 16 because of FW
    final writeNetwork = device!.writeNetwork;

    try {
      int from = 0;
      int to = chunkSize;

      //_log.i('Resetting OTA on network');
      final start = DateTime.now();
      await _writeBytes(metadataParser.networkBytes(bytes: metadataParser.otaReset()), writeNetwork);
      await _sleep(100);

      messenger.emitOtaStatus("begin");
      await _writeBytes(metadataParser.networkBytes(bytes: metadataParser.otaBegin(firmware: bytes)), writeNetwork,
          timeout: 20000);
      //_log.i('Waiting 10s to let ESP erase the flash');
      await _sleep(10000);

      //_log.i('Writing OTA bytes');
      int written = 0;
      while (written < bytes.length) {
        if (to > bytes.length) {
          to = bytes.length;
        }
        final Uint8List packet = metadataParser.otaWrite(
          firmware: bytes,
          written: written,
          from: from,
          to: to,
        );
        await _writeBytes(metadataParser.networkBytes(bytes: packet), writeNetwork);
        written += to - from;
        from += chunkSize;
        to = from + chunkSize;

        final int percentage = (((10000 * written) / bytes.length) / 100).round();
        messenger.emitOtaProgress(percentage);
        yield percentage;
      }

      await _sleep(100);
      //_log.i('Ending OTA update');
      await _writeBytes(metadataParser.networkBytes(bytes: metadataParser.otaEnd(written: written)), writeNetwork);
      final end = DateTime.now();
      final updateDuration = end.difference(start);
      if (kDebugMode) {
        print('UpdateDuration: $updateDuration');
      }

      await _sleep(1000);
      //_log.i('Rebooting whole network...');
      await _writeBytes(metadataParser.rebootBytes(), writeNetwork);
      //_log.i('Updating NETWORK firmware done');
      messenger.emitOtaStatus("success");
      messenger.resolve();
    } catch (e, trace) {
      messenger.emitOtaStatus("fail");
      messenger.rejectAs("UpdateFailed: $e, $trace");
      return;
    }
  }

  Future<void> setNetworkNotificationListener(void Function(List<int>) callback) async {
    handleNetworkNotification = callback;
  }

  Future<void> setClockNotificationListener(void Function(List<int>) callback) async {
    handleClockNotification = callback;
  }

  Future<void> setDeviceNotificationListener(void Function(List<int>) callback) async {
    handleDeviceNotification = callback;
  }

  Future<void> _listenNotifications() async {
    final networkStream = await device?.streamNetwork();
    final clockStream = await device?.streamClock();
    final deviceStream = await device?.streamDevice();
    await _cancelCharSubscriptions();
    networkCharSubscription = networkStream?.listen((bytes) {
      messenger.notifyNetwork(bytes);
      handleNetworkNotification?.call(bytes);
    });
    clockCharSubscription = clockStream?.listen((bytes) {
      messenger.notifyClock(bytes);
      handleClockNotification?.call(bytes);
    });
    deviceCharSubscription = deviceStream?.listen((bytes) {
      messenger.notifyDevice(bytes);
      handleDeviceNotification?.call(bytes);
    });
  }

  Future<void> _listenDeviceState() async {
    await _cancelDeviceSubscription();
    deviceSubscription = device?.state.listen((state) async {
      switch (state) {
        // For some reason device NEVER in "connecting" state
        case BluetoothDeviceState.connecting:
        case BluetoothDeviceState.disconnecting:
          // nothing
          break;
        case BluetoothDeviceState.connected:
          // Already emitted
          break;
        // Also triggered when listener is initialized
        case BluetoothDeviceState.disconnected:
          if (isConnected) {
            isConnected = false;
            await _cancelCharSubscriptions();
            messenger.emitDisconnected();
          }
          break;
        default:
          throw UnimplementedError('Unknown BluetoothDeviceState "$state"');
      }
    });
  }

  Future<void> _writeBytes(
      Uint8List bytes, Future<void> Function(Uint8List, {bool withoutResponse, int? timeout}) write,
      {bool withoutResponse = false, int? timeout}) async {
    final int uuid = nextUUID();
    final int packetSize = device!.mtu; // min size 13
    if (packetSize < 13) {
      throw Exception('MTU is lower then 13, currently: $packetSize');
    }

    final int bytesSize = packetSize - 12;
    int from = 0;
    int to = bytesSize;

    while (from < bytes.length) {
      if (to > bytes.length) {
        to = bytes.length;
      }
      final Uint8List packet = metadataParser.packetBytes(
        payload: bytes,
        uuid: uuid,
        from: from,
        to: to,
      );
      await write(packet, withoutResponse: withoutResponse, timeout: timeout);
      from += bytesSize;
      to = from + bytesSize;
    }
  }

  Future<void> _cancelDeviceSubscription() async {
    await deviceSubscription?.cancel();
    deviceSubscription = null;
  }

  Future<void> _cleanSyncCommunication() async {
    await device!.writeClock(Uint8List.fromList([]));
  }

  Future<void> _sleep(int millis) async {
    await Future.delayed(Duration(milliseconds: millis));
  }

  Future<void> _cancelCharSubscriptions() async {
    await networkCharSubscription?.cancel();
    await clockCharSubscription?.cancel();
    await deviceCharSubscription?.cancel();
    networkCharSubscription = null;
    clockCharSubscription = null;
    deviceCharSubscription = null;
    handleNetworkNotification = null;
    handleClockNotification = null;
    handleDeviceNotification = null;
  }

  int nextUUID() {
    if (_uuid >= maxUUID) {
      _uuid = 1;
    }
    return _uuid++;
  }
}
