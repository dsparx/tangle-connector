import 'package:flutter/material.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/bluetooth_connection_service.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/filter_data.dart';
import 'package:flutter_connector/src/gps_service.dart';
import 'package:flutter_connector/src/tangle_device.dart';
import 'package:flutter_connector/src/widgets/my_futurebuilder.dart';
import 'package:flutter_connector/src/widgets/my_streambuilder.dart';

class TangleScanner extends StatelessWidget {
  final BluetoothConnectionService bluetooth;
  final GpsService gps;
  final Widget Function(Iterable<TangleDevice> devices, bool isScanning, Future<void> Function() startScan) builder;

  final FilterData filter;
  final int timeout;
  final bool disconnectConnectedDevices;
  final Widget onLoading;
  final Widget Function(Object?) onError;

  const TangleScanner({
    Key? key,
    required this.bluetooth,
    required this.gps,
    required this.builder,
    required this.onLoading,
    required this.onError,
    required this.filter,
    required this.timeout,
    required this.disconnectConnectedDevices,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Future<void> startScan() => bluetooth.startScan(
          timeout: timeout,
          disconnectConnectedDevices: disconnectConnectedDevices,
        );
    return MyFutureBuilder(
      future: startScan(),
      onLoading: onLoading,
      onError: onError,
      builder: (_) {
        return MyStreamBuilder<Iterable<TangleDevice>>(
          stream: bluetooth.scanResults(filterData: filter),
          initialData: const [],
          builder: (devices) {
            return MyStreamBuilder<bool>(
              stream: bluetooth.isScanning,
              initialData: true,
              builder: (isScanning) {
                return builder(devices, isScanning, startScan);
              },
            );
          },
        );
      },
    );
  }
}
