import 'package:flutter/material.dart';

class MyFutureBuilder<T> extends StatelessWidget {
  final Future<T> future;
  final Widget Function(T) builder;
  final Widget Function(Object?) onError;
  final Widget onLoading;

  const MyFutureBuilder({
    Key? key,
    required this.future,
    required this.builder,
    required this.onError,
    required this.onLoading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return onError(snapshot.error);
        }
        if (snapshot.connectionState == ConnectionState.done) {
          return builder(snapshot.data as T);
        }
        return onLoading;
      },
    );
  }
}
