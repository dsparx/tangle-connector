import 'package:flutter/foundation.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_connector/flutter_connector.dart';

enum AppBluetoothState {
  blOff,
  blOn,
  blUnavailable,
}

abstract class BluetoothStateManager extends ChangeNotifier {
  AppBluetoothState get state;
}

class BluetoothStateManagerImpl extends ChangeNotifier implements BluetoothStateManager {
  //final _log = getLogger('BluetoothStateManager');

  final BluetoothConnectionService bluetoothService;
  final TangleConnector connector;

  @override
  AppBluetoothState state = AppBluetoothState.blOff;

  BluetoothStateManagerImpl({
    required this.bluetoothService,
    required this.connector,
  }) {
    setBluetoothStateListener();
  }

  void setBluetoothStateListener() {
    bluetoothService.state.listen((state) async {
      //_log.i('BluetoothState changed: $state');
      switch (state) {
        case BluetoothState.turningOn:
          // nothing
          break;
        case BluetoothState.on:
          setState(AppBluetoothState.blOn);
          break;
        case BluetoothState.off:
        case BluetoothState.turningOff:
          await connector.ensureDisconnectedWithoutResolve();
          setState(AppBluetoothState.blOff);
          break;
        // Workaround to: https://github.com/boskokg/flutter_blue_plus/issues/66
        // On iOS the BLE state is sometimes wrongly set at unknown, even tho
        // it is working, that's why we alter BLE.unknown as AppBluetoothState.blOn
        case BluetoothState.unknown:
          if (defaultTargetPlatform == TargetPlatform.iOS) {
            setState(AppBluetoothState.blOn);
          } else {
            setState(AppBluetoothState.blUnavailable);
          }
          break;
        case BluetoothState.unavailable:
          setState(AppBluetoothState.blUnavailable);
          break;
        case BluetoothState.unauthorized:
          throw Exception('Bluetooth in unauthorized');
        default:
          throw UnimplementedError('Unknown BluetoothState "$state"');
      }
    });
  }

  void setState(AppBluetoothState newState) {
    if (state != newState) {
      //_log.i('New state "$newState"');
      state = newState;
      notifyListeners();
    }
  }
}
