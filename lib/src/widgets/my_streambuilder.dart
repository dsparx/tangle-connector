import 'package:flutter/material.dart';

class MyStreamBuilder<T> extends StatelessWidget {
  final Stream stream;
  final T initialData;
  final Widget Function(T) builder;
  final Widget? onError;
  final Widget? onLoading;

  const MyStreamBuilder({
    Key? key,
    required this.stream,
    required this.initialData,
    required this.builder,
    this.onError,
    this.onLoading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: stream,
      initialData: initialData,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: onError ??
                Text(
                  snapshot.error.toString(),
                ),
          );
        }
        if (snapshot.hasData) {
          return builder(snapshot.data as T);
        }
        return onLoading ?? const Center(child: CircularProgressIndicator());
      },
    );
  }
}
