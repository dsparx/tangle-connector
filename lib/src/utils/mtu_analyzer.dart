import 'dart:math';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/foundation.dart';

class MtuAnalyzer {
  final deviceInfo = DeviceInfoPlugin();

  Future<int> mtuWithPlatformUpperBound(int mtu) async {
    if (defaultTargetPlatform == TargetPlatform.iOS) {
      // iPhone devices make MTU request with their maximal MTU,
      // therefore we just need to give it a max cap of 512
      return min(mtu, 512);
    } else {
      // Android devices make no MTU request, therefore we need
      // to "guess" what is the maximal MTU of the Android device
      final androidSdk = await androidSdkVersion();
      // SDK 26 = Android 8.0
      if (androidSdk >= 26) {
        // MTU 512 working on newer Android phones
        return min(mtu, 512);
      } else {
        return min(mtu, 185);
      }
    }
  }

  Future<int> androidSdkVersion() async {
    assert(defaultTargetPlatform == TargetPlatform.android);
    final androidInfo = await deviceInfo.androidInfo;
    final sdkVersion = androidInfo.version.sdkInt;
    return sdkVersion;
  }
}
