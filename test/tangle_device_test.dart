import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'mocks/mockito.mocks.dart';

void main() {
  // const serviceUUID = '0000ffe0-0000-1000-8000-00805f9b34fb';
  const terminalCharacteristicUUID = '0000ffe1-0000-1000-8000-00805f9b34fb';
  const clockCharacteristicUUID = '0000ffe2-0000-1000-8000-00805f9b34fb';
  const deviceCharacteristicUUID = '0000ffe3-0000-1000-8000-00805f9b34fb';

  // late BluetoothDevice deviceMock;
  late BluetoothCharacteristic terminalChar;
  late BluetoothCharacteristic clockChar;
  late BluetoothCharacteristic deviceChar;

  setUp(() {
    // deviceMock = MockBluetoothDevice();
    terminalChar = MockBluetoothCharacteristic();
    clockChar = MockBluetoothCharacteristic();
    deviceChar = MockBluetoothCharacteristic();

    when(terminalChar.uuid).thenReturn(Guid(terminalCharacteristicUUID));
    when(clockChar.uuid).thenReturn(Guid(clockCharacteristicUUID));
    when(deviceChar.uuid).thenReturn(Guid(deviceCharacteristicUUID));
  });

  // TangleDevice _initTangleDevice(BluetoothDevice device) => TangleDevice(
  //   scanResult: ,
  //   serviceUUID: Guid(serviceUUID),
  //   networkCharacteristicUUID: Guid(terminalCharacteristicUUID),
  //   clockCharacteristicUUID: Guid(clockCharacteristicUUID),
  //   deviceCharacteristicUUID: Guid(deviceCharacteristicUUID),
  //   discoverServicesTimeout: 50,
  //   writeTimeout: 100,
  // );

  // MockBluetoothService serviceMock(Guid guid) {
  //   final serviceMock = MockBluetoothService();
  //   when(serviceMock.uuid).thenReturn(guid);
  //   when(serviceMock.characteristics).thenReturn([terminalChar, clockChar, deviceChar]);
  //   return serviceMock;
  // }

  // test('Device state', () async {
  //   when(deviceMock.state).thenAnswer((_) => Stream.fromIterable([BluetoothDeviceState.disconnected]));
  //   final tangleDevice = _initTangleDevice(deviceMock);
  //   expect(await tangleDevice.state.first, BluetoothDeviceState.disconnected);
  // });

  // group('Constructor', () {
  //   test('has Tangle name', () async {
  //     when(deviceMock.name).thenReturn('Tangle');
  //     when(deviceMock.state).thenAnswer((_) => Stream.fromIterable([BluetoothDeviceState.disconnected]));
  //     final tangleDevice = _initTangleDevice(deviceMock);
  //     expect(tangleDevice.name, 'Tangle');
  //   });
  // });

  // group('Connect', () {
  //   test('Valid', () async {
  //     when(deviceMock.mtu).thenAnswer((_) => Stream.fromIterable([512]));
  //     when(deviceMock.state).thenAnswer((_) => Stream.fromIterable([BluetoothDeviceState.disconnected]));
  //     when(deviceMock.disconnect()).thenAnswer((_) async => null);
  //     when(deviceMock.discoverServices()).thenAnswer((_) async => [serviceMock(Guid(serviceUUID))]);

  //     final tangleDevice = _initTangleDevice(deviceMock);
  //     await tangleDevice.connect();

  //     await tangleDevice.disconnect();
  //   });

  //   test('Discover global.services timeout', () async {
  //     when(deviceMock.state).thenAnswer((_) => Stream.fromIterable([BluetoothDeviceState.disconnected]));
  //     when(deviceMock.disconnect()).thenAnswer((_) async => 0);
  //     when(deviceMock.discoverServices()).thenThrow(TimeoutException);

  //     final tangleDevice = _initTangleDevice(deviceMock);
  //     expect(() => tangleDevice.connect(), throwsException);
  //   });

  //   test('Invalid Service', () async {
  //     when(deviceMock.state).thenAnswer((_) => Stream.fromIterable([BluetoothDeviceState.disconnected]));
  //     when(deviceMock.disconnect()).thenAnswer((_) async => 0);
  //     when(deviceMock.discoverServices()).thenAnswer((_) async => [serviceMock(Guid.empty())]);

  //     final tangleDevice = _initTangleDevice(deviceMock);
  //     expect(() => tangleDevice.connect(), throwsException);
  //   });
  // });

  // group('Write to characteristic', () {
  //   test('terminal, throw exception', () async {
  //     when(deviceMock.mtu).thenAnswer((_) => Stream.fromIterable([512]));
  //     when(deviceMock.state).thenAnswer((_) => Stream.fromIterable([BluetoothDeviceState.disconnected]));
  //     when(deviceMock.disconnect()).thenAnswer((_) async => null);
  //     when(terminalChar.write([])).thenThrow(PlatformException);
  //     when(deviceMock.discoverServices()).thenAnswer((_) async {
  //       final BluetoothService serviceMock = MockBluetoothService();
  //       when(serviceMock.uuid).thenReturn(Guid(serviceUUID));
  //       when(serviceMock.characteristics).thenReturn([terminalChar, clockChar, deviceChar]);
  //       return [serviceMock];
  //     });
  //     final tangleDevice = _initTangleDevice(deviceMock);
  //     await tangleDevice.connect();
  //     expect(() => tangleDevice.writeNetwork(Uint8List.fromList([])), throwsException);
  //   });

  //   test('clock, throw exception', () async {
  //     when(deviceMock.mtu).thenAnswer((_) => Stream.fromIterable([512]));
  //     when(deviceMock.state).thenAnswer((_) => Stream.fromIterable([BluetoothDeviceState.disconnected]));
  //     when(deviceMock.disconnect()).thenAnswer((_) async => null);
  //     when(clockChar.write([], withoutResponse: true)).thenThrow(PlatformException);
  //     when(deviceMock.discoverServices()).thenAnswer((_) async {
  //       final BluetoothService serviceMock = MockBluetoothService();
  //       when(serviceMock.uuid).thenReturn(Guid(serviceUUID));
  //       when(serviceMock.characteristics).thenReturn([terminalChar, clockChar, deviceChar]);
  //       return [serviceMock];
  //     });
  //     final tangleDevice = _initTangleDevice(deviceMock);
  //     await tangleDevice.connect();
  //     expect(() => tangleDevice.writeClock(Uint8List.fromList([])), throwsException);
  //   });

  //   test('device, throw exception', () async {
  //     when(deviceMock.mtu).thenAnswer((_) => Stream.fromIterable([512]));
  //     when(deviceMock.state).thenAnswer((_) => Stream.fromIterable([BluetoothDeviceState.disconnected]));
  //     when(deviceMock.disconnect()).thenAnswer((_) async => null);
  //     when(deviceChar.write([], withoutResponse: true)).thenThrow(PlatformException);
  //     when(deviceMock.discoverServices()).thenAnswer((_) async {
  //       final BluetoothService serviceMock = MockBluetoothService();
  //       when(serviceMock.uuid).thenReturn(Guid(serviceUUID));
  //       when(serviceMock.characteristics).thenReturn([terminalChar, clockChar, deviceChar]);
  //       return [serviceMock];
  //     });
  //     final tangleDevice = _initTangleDevice(deviceMock);
  //     await tangleDevice.connect();
  //     expect(() => tangleDevice.writeDevice(Uint8List.fromList([])), throwsException);
  //   });
  // });
}
