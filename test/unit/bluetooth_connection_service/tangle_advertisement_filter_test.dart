import 'dart:typed_data';

import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/manufacturer_data.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/tangle_advertisement_filter.dart';

void main() {
  final TangleAdvertisementFilterImpl filter = TangleAdvertisementFilterImpl();

  Uint8List _listFromZero(int length) => Uint8List.fromList(List.generate(length, (i) => i));

  group('name', () {
    test('matches', () async {
      expect(filter.verifyName(name: 'Light', nameToVerify: 'Light'), true);
    });

    test('does not match', () async {
      expect(filter.verifyName(name: 'Light', nameToVerify: 'Light0'), false);
    });
  });

  group('namePrefix', () {
    test('has', () async {
      expect(filter.verifyNamePrefix(prefix: 'Light', nameToVerify: 'Light0'), true);
    });

    test('does not have', () async {
      expect(filter.verifyNamePrefix(prefix: 'Light', nameToVerify: 'TLight0'), false);
    });
  });

  group('restrictedIdentifiers', () {
    test('if contains returns false', () async {
      expect(
        filter.verifyRestrictedIdentifiers(
          {
            const DeviceIdentifier('1'),
            const DeviceIdentifier('2'),
          },
          const DeviceIdentifier('2'),
        ),
        false,
      );
    });

    test('if does not contain returns true', () async {
      expect(
        filter.verifyRestrictedIdentifiers(
          {
            const DeviceIdentifier('1'),
            const DeviceIdentifier('2'),
          },
          const DeviceIdentifier('7'),
        ),
        true,
      );
    });
  });

  group('manufacturerData', () {
    test('null', () async {
      expect(filter.verifyManufacturerData(null, {123: List.generate(21, (i) => i)}), true);
    });

    test('all params null', () async {
      expect(
        filter.verifyManufacturerData(
          ManufacturerData(),
          {123: List.generate(21, (i) => i)},
        ),
        true,
      );
    });

    test('all params non-null', () async {
      expect(
        filter.verifyManufacturerData(
          ManufacturerData(
            fwVersion: '0.8.2',
            productCode: 65534,
            ownerSignatureHash: _listFromZero(32),
            adoption: true,
          ),
          {
            123: [34, 3, 254, 255, ..._listFromZero(16), 1]
          },
        ),
        true,
      );
    });
  });
}
