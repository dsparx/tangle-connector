import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/manufacturer_data.dart';

void main() {
  Uint8List _listFromZero(int length) => Uint8List.fromList(List.generate(length, (i) => i));

  group('ManufacturerData', () {
    test('bytesToCode', () async {
      expect(ManufacturerData.bytes16ToCode([254, 255]), 65534);
    });

    test('bytesToFwVersion', () async {
      expect(ManufacturerData.bytesToFwVersion([34, 3]), '0.8.2');
    });

    test('fromData', () async {
      final data = ManufacturerData.fromData({
        731: [34, 3, 254, 255, ..._listFromZero(16), 1]
      });
      expect(data.fwVersion, '0.8.2');
      expect(data.productCode, 65534);
      expect(data.ownerSignatureHash, _listFromZero(16));
      expect(data.adoption, true);
    });

    group('matches', () {
      test('arguments null', () async {
        final data = ManufacturerData.fromData({
          731: [33, 3, 254, 255, ..._listFromZero(16), 1]
        });
        expect(data.matchesFwVersion(null), true);
        expect(data.matchesFwVersion(null), true);
        expect(data.matchesProductCode(null), true);
        expect(data.matchesOwnerSignature(null), true);
        expect(data.matchesAdoptionFlag(null), true);
      });

      group('fwVersion', () {
        test('matches', () async {
          final data = ManufacturerData.fromData({
            731: [33, 3, 254, 255, ..._listFromZero(16), 1]
          });
          expect(data.matchesFwVersion('0.8.1'), true);
          expect(data.matchesFwVersion('0.8.2'), false);
        });

        test('unmatches', () async {
          final data = ManufacturerData.fromData({
            731: [33, 3, 254, 255, ..._listFromZero(16), 1]
          });
          expect(data.unmatchesFwVersion('0.8.1'), false);
          expect(data.unmatchesFwVersion('0.8.2'), true);
        });
      });

      test('productCode', () async {
        final data = ManufacturerData.fromData({
          731: [33, 3, 254, 255, ..._listFromZero(16), 1]
        });
        expect(data.matchesProductCode(65534), true);
      });

      test('ownerSignature', () async {
        final data = ManufacturerData.fromData({
          731: [33, 3, 254, 255, ..._listFromZero(16), 1]
        });
        expect(data.matchesOwnerSignature(_listFromZero(32)), true);
      });

      group('adoptionFlag', () {
        test('true', () async {
          final data = ManufacturerData.fromData({
            731: [33, 3, 254, 255, ..._listFromZero(16), 1]
          });
          expect(data.matchesAdoptionFlag(true), true);
          expect(data.matchesAdoptionFlag(false), false);
        });

        test('false', () async {
          final data = ManufacturerData.fromData({
            731: [33, 3, 254, 255, ..._listFromZero(16), 0]
          });
          expect(data.matchesAdoptionFlag(false), true);
          expect(data.matchesAdoptionFlag(true), false);
        });
      });
    });
  });
}
