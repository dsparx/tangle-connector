import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_connector/src/bluetooth_connection_service/filter_data.dart';

void main() {
  group('FilterData', () {
    test('matches', () async {
      final filter = FilterData.fromJson(jsonDecode('''
        [
          {
            "name": "A"
          },
          {
            "name": "B"
          }
        ]
      '''));
      expect(filter.filters.elementAt(0).name, 'A');
      expect(filter.filters.elementAt(1).name, 'B');
    });

    test('skips legacy', () async {
      final filter = FilterData.fromJson(jsonDecode('''
        [
          {
            "name": "A"
          },
          {
            "legacy": true
          }
        ]
      '''));
      expect(filter.filters.length, 1);
    });
  });

  group('FilterItem', () {
    test('matches', () async {
      final filter = FilterItem.fromJson(jsonDecode('''
        {
          "name": "NARA Alpha",
          "namePrefix": "NARA",
          "fwVersion": "!0.7.3",
          "productCode": 2,
          "adoptionFlag": true
        }
      '''));
      expect(filter.name, 'NARA Alpha');
      expect(filter.namePrefix, 'NARA');

      final manufacturerData = filter.manufacturerData;
      expect(manufacturerData?.fwVersion, '!0.7.3');
      expect(manufacturerData?.ownerSignatureHash, null);
      expect(manufacturerData?.productCode, 2);
      expect(manufacturerData?.adoption, true);
    });
  });
}
