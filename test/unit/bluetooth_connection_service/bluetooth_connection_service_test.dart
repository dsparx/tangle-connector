import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../mocks/mockito.mocks.dart';

void main() {
  late FlutterBluePlus flutterBlueMock;
  // late BluetoothConnectionService ble;
  // late GpsService gps;
  // final ownerSignature = List.generate(16, (i) => i);
  // const fwVersionBytes = [33, 3];
  // const productCodeBytes = [1, 2];

  setUp(() {
    flutterBlueMock = MockFlutterBluePlus();
    // gps = MockGpsService();
    when(flutterBlueMock.state).thenAnswer((_) => Stream.value(BluetoothState.off));

    // ble = BluetoothConnectionServiceImpl(
    //   flutterBlue: flutterBlueMock,
    //   gpsService: gps,
    //   serviceGUID: Guid('0000ffe0-0000-1000-8000-00805f9b34fb'),
    //   serviceAdoptGUID: Guid('723247e6-3e2d-4279-ad8e-85a13b74d4a5'),
    //   networkCharacteristicGUID: Guid('0000ffe1-0000-1000-8000-00805f9b34fb'),
    //   clockCharacteristicGUID: Guid('0000ffe2-0000-1000-8000-00805f9b34fb'),
    //   deviceCharacteristicGUID: Guid('0000ffe3-0000-1000-8000-00805f9b34fb'),
    //   discoverServicesTimeout: 50,
    // );
  });

  // ScanResult _createScanResult({required List<int> manufacturerData}) {
  //   final pb.ScanResult scanResultProto = pb.ScanResult.create();
  //   final pb.BluetoothDevice deviceProto = pb.BluetoothDevice.create();
  //   final pb.AdvertisementData advertisementData = pb.AdvertisementData(
  //     manufacturerData: {123: manufacturerData},
  //   );
  //   scanResultProto.device = deviceProto;
  //   scanResultProto.advertisementData = advertisementData;
  //   return ScanResult.fromProto(scanResultProto);
  // }

  // test('Scan finds owned device', () async {
  //   when(flutterBlueMock.isScanning).thenAnswer((_) async* {
  //     yield true;
  //   });
  //   when(flutterBlueMock.connectedDevices).thenAnswer((_) async => []);
  //   when(flutterBlueMock.scanResults).thenAnswer((_) async* {
  //     yield [
  //       _createScanResult(
  //         manufacturerData: [...fwVersionBytes, ...productCodeBytes, ...ownerSignature, 0],
  //       )
  //     ];
  //   });
  //
  //   final devices = await ble.scanResults(filterData: FilterData.empty()).first;
  //   expect(devices.length, 1);
  // });

  // test('device in connectedDevices', () async {
  //   when(flutterBlueMock.connectedDevices).thenAnswer((_) async {
  //     final device = MockBluetoothDevice();
  //     when(device.name).thenReturn('Tangle');
  //     when(device.state).thenAnswer((_) => Stream.value(BluetoothDeviceState.disconnected));
  //     return [device];
  //   });

  //   final Stream<TangleDevicesDTO> devices = service.scanTangleDevice(ownerSignature: Uint8List.fromList(List.generate(16, (i) => i)));
  //   final first = await devices.first;
  //   expect(first.owned, 'Tangle');
  // });

  /*test('device in scanResult', () async {
    when(flutterBlueMock.startScan(timeout: const Duration(milliseconds: _delayMillis))).thenAnswer((_) async => null);
    when(flutterBlueMock.connectedDevices).thenAnswer((_) async => []);
    when(flutterBlueMock.scanResults).thenAnswer((_) async* {
      final pb.ScanResult scanResultProto = pb.ScanResult.create();
      final pb.BluetoothDevice deviceProto = pb.BluetoothDevice.create();
      deviceProto.name = 'Tangle';
      scanResultProto.device = deviceProto;
      yield [ScanResult.fromProto(scanResultProto)];
    });
    when(flutterBlueMock.stopScan()).thenAnswer((_) async => null);

    await service.findTangleDevice();
    final TangleDevice device = await service.findTangleDevice();
    expect(device.name, 'Tangle');
  });

  group('Test method bluetoothStateStream: ', () {
    test('bluetooth is on', () async {
      when(flutterBlueMock.state).thenAnswer(
          (_) => Stream.fromIterable([BluetoothState.on]));

      final List<bool> matcherStreamValues = [true];
      await expectLater(service.bluetoothStateStream,
          emitsInOrder(matcherStreamValues));
    });

    test('bluetooth is off', () async {
      when(flutterBlueMock.state).thenAnswer(
          (_) => Stream.fromIterable([BluetoothState.off]));
      final List<bool> matcherStreamValues = [false];
      await expectLater(service.bluetoothStateStream,
          emitsInOrder(matcherStreamValues));
    });

    test('bluetooth is switching between on and off', () async {
      when(flutterBlueMock.state).thenAnswer((_) =>
          Stream.fromIterable([
            BluetoothState.on,
            BluetoothState.off,
            BluetoothState.on,
            BluetoothState.off
          ]));
      final List<bool> matcherStreamValues = [true, false, true, false];
      await expectLater(service.bluetoothStateStream,
          emitsInOrder(matcherStreamValues));
    });
  });*/
}
