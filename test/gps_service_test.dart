import 'package:flutter_test/flutter_test.dart';
import 'package:location/location.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_connector/src/gps_service.dart';

import 'mocks/mockito.mocks.dart';

void main() {
  final location = MockLocation();
  final service = GpsService(locationService: location);

  group('GPS', () {
    test('ensurePermissionGrantedAndServiceEnabled', () async {
      when(location.hasPermission()).thenAnswer((_) async => PermissionStatus.granted);
      expect(() async => service.ensurePermissionGranted(), returnsNormally);
      verifyNever(location.requestPermission());
    });

    group('ensurePermissionGranted', () {
      group('success: ', () {
        test('permission granted', () async {
          when(location.hasPermission()).thenAnswer((_) async => PermissionStatus.granted);
          expect(() async => service.ensurePermissionGranted(), returnsNormally);
          verifyNever(location.requestPermission());
        });

        test('permission grantedLimited', () async {
          when(location.hasPermission()).thenAnswer((_) async => PermissionStatus.grantedLimited);
          expect(() async => service.ensurePermissionGranted(), returnsNormally);
          verifyNever(location.requestPermission());
        });

        test('permission denied, after request granted', () async {
          when(location.hasPermission()).thenAnswer((_) async => PermissionStatus.denied);
          when(location.requestPermission()).thenAnswer((_) async => PermissionStatus.granted);
          expect(() async => service.ensurePermissionGranted(), returnsNormally);
        });

        test('permission deniedForever, after request grantedLimited', () async {
          when(location.hasPermission()).thenAnswer((_) async => PermissionStatus.deniedForever);
          when(location.requestPermission()).thenAnswer((_) async => PermissionStatus.grantedLimited);
          expect(() async => service.ensurePermissionGranted(), returnsNormally);
        });
      });

      group('error: ', () {
        test('permission denied, after request denied', () async {
          when(location.hasPermission()).thenAnswer((_) async => PermissionStatus.denied);
          when(location.requestPermission()).thenAnswer((_) async => PermissionStatus.denied);
          expect(() async => service.ensurePermissionGranted(), throwsStateError);
        });

        test('permission deniedForever, after request deniedForever', () async {
          when(location.hasPermission()).thenAnswer((_) async => PermissionStatus.deniedForever);
          when(location.requestPermission()).thenAnswer((_) async => PermissionStatus.deniedForever);
          expect(() async => service.ensurePermissionGranted(), throwsStateError);
        });
      });
    });

    group('ensureServiceEnabled', () {
      group('success: ', () {
        test('permission granted', () async {
          when(location.serviceEnabled()).thenAnswer((_) async => true);
          expect(() async => service.ensureServiceEnabled(), returnsNormally);
        });

        test('permission denied, after request granted', () async {
          when(location.serviceEnabled()).thenAnswer((_) async => false);
          when(location.requestService()).thenAnswer((_) async => true);
          expect(() async => service.ensureServiceEnabled(), returnsNormally);
        });
      });

      group('error: ', () {
        test('permission denied, after request denied', () async {
          when(location.serviceEnabled()).thenAnswer((_) async => false);
          when(location.requestService()).thenAnswer((_) async => false);
          expect(() async => service.ensureServiceEnabled(), throwsStateError);
        });
      });
    });
  });
}
