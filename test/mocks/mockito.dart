import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_connector/flutter_connector.dart';
import 'package:location/location.dart';
import 'package:mockito/annotations.dart';

@GenerateMocks([
  FlutterBluePlus,
  Location,
  GpsService,
  BluetoothDevice,
  BluetoothService,
  BluetoothCharacteristic,
])
void main() {}
